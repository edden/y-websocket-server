const express = require('express');
const ws = require('ws');
const setupWSConnection = require('./utils.js').setupWSConnection

const app = express();


const wss = new ws.Server({ noServer: true })
wss.on('connection', setupWSConnection)


app.get('/alive', function (req, res) {
  res.send('true')
})

const server = app.listen(1234);

server.on('upgrade', (request, socket, head) => {
  wss.handleUpgrade(request, socket, head, socket => {
    wss.emit('connection', socket, request);
  });
});
